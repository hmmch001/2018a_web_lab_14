package ictgradschool.web.lab14.ex03;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.Map;

public class ImageGalleryDisplay extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><title>Image Gallery</title>");
        out.println("</head><body>");
        out.println("<h1>Image Gallery</h1>");
        out.println("<table><thead><tr><th>Thumbnail</th><th>Filename</th><th>File-Size</th></tr></thead><tbody>");
        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");
        File photos = new File(fullPhotoPath);

        for (File photo : photos.listFiles()){
            String name = photo.getName();
            if(name.endsWith("_thumbnail.png")) {
                int x = name.lastIndexOf('_');
                String fullSizeName = name.substring(0, x);
                out.println("<tr><td><a href =\"/Photos/" + fullSizeName + ".jpg\"><img src = \"/Photos/" + name + "\"></a><td>");
                String fileName = fullSizeName.replaceAll("_", " ");
                out.println("<td>" + fileName + "</td>");
                long size = new File(fullPhotoPath + "/"+ fullSizeName + ".jpg").length();
                out.println("<td>" + size + "</td></tr>");
            }
        }
        out.println("</tbody></table></body></html>");
    }




}
